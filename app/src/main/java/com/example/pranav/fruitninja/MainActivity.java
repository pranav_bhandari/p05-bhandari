package com.example.pranav.fruitninja;

import android.app.Activity;
import android.content.res.Resources;
import android.graphics.Matrix;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Size;
import android.view.Display;
import android.view.MotionEvent;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.LinearInterpolator;
import android.view.animation.RotateAnimation;
import android.view.animation.TranslateAnimation;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;

public class MainActivity extends Activity{
    private Animation mAnimation;
    private  int iClicks = 0,scr = 0;
    ImageView img,img2,img3;
    ImageView[] imgV = new ImageView[10];
    TextView score;
    public float x_value=0,y=0,xv2=0,y2=0,xv3=0,y3=0;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        score = (TextView) findViewById(R.id.score);
        final Display display = getWindowManager().getDefaultDisplay();
        //timer
        Timer timer = new Timer();
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        // task to be done every 1000 milliseconds
                        iClicks = iClicks + 1;
                        if(y == Resources.getSystem().getDisplayMetrics().heightPixels)
                            y = 0;
                        y += (Resources.getSystem().getDisplayMetrics().heightPixels)/10;
                        if(y2 == Resources.getSystem().getDisplayMetrics().heightPixels)
                            y2 = 0;
                        y2 += (Resources.getSystem().getDisplayMetrics().heightPixels)/10;
                        if(y3 == Resources.getSystem().getDisplayMetrics().heightPixels)
                            y3 = 0;
                        y3 += (Resources.getSystem().getDisplayMetrics().heightPixels)/10;


                        //generating fruits each secs
                       // displayImages();

                    }
                });

            }
        }, 0, 1000);

        displayImages();
        displayImage2();
        displayImage3();


    }

    private void displayImage3() {
        img3=(ImageView)findViewById(R.id.imageView3);
        String[] imageArray = {"watermelon","apple","orange","strawberry"};
        Random rand = new Random();
        Random sx = new Random();
        int x = sx.nextInt(720 - 0 + 1);
        img3.setX(x);
        xv3 = x;
        int rndInt = rand.nextInt(4);
        int resID = getResources().getIdentifier(imageArray[rndInt], "drawable",  getPackageName());
        img3.setImageResource(resID);



        mAnimation = new TranslateAnimation(
                TranslateAnimation.ABSOLUTE, 0f,
                TranslateAnimation.ABSOLUTE, 0f,
                TranslateAnimation.RELATIVE_TO_PARENT, 0f,
                TranslateAnimation.RELATIVE_TO_PARENT, 1.0f);
        mAnimation.setDuration(20000);
        mAnimation.setRepeatCount(-1);

        // mAnimation.setRepeatMode(Animation.REVERSE);
        mAnimation.setInterpolator(new LinearInterpolator());
        img3.setAnimation(mAnimation);
    }

    private void displayImage2() {

        img2=(ImageView)findViewById(R.id.imageView2);
        String[] imageArray = {"watermelon","apple","orange","strawberry"};
        Random rand = new Random();
        Random sx = new Random();
        int x = sx.nextInt(720 - 0 + 1);
        img2.setX(x);
        xv2 = x;
        int rndInt = rand.nextInt(4);
        int resID = getResources().getIdentifier(imageArray[rndInt], "drawable",  getPackageName());
        img2.setImageResource(resID);



        mAnimation = new TranslateAnimation(
                TranslateAnimation.ABSOLUTE, 0f,
                TranslateAnimation.ABSOLUTE, 0f,
                TranslateAnimation.RELATIVE_TO_PARENT, 0f,
                TranslateAnimation.RELATIVE_TO_PARENT, 1.0f);
        mAnimation.setDuration(15000);
        mAnimation.setRepeatCount(-1);

        // mAnimation.setRepeatMode(Animation.REVERSE);
        mAnimation.setInterpolator(new LinearInterpolator());
        img2.setAnimation(mAnimation);


    }


    public void displayImages()
    {
        Random rn = new Random();

        int r = rn.nextInt(10);
        for (int i = 0; i < 3;i++)
        {
//            imgV[i]=(ImageView)findViewById(R.id.imageView1);
//            String[] imageArray = {"watermelon","apple","orange"};
//            Random rand = new Random();
//            Random sx = new Random();
//            int x = sx.nextInt(720 - 0 + 1);
//            imgV[i].setX(x);
//
//            int rndInt = rand.nextInt(3);
//            int resID = getResources().getIdentifier(imageArray[rndInt], "drawable",  getPackageName());
//            imgV[i].setImageResource(resID);



//            mAnimation = new TranslateAnimation(
//                    TranslateAnimation.ABSOLUTE, 0f,
//                    TranslateAnimation.ABSOLUTE, 0f,
//                    TranslateAnimation.RELATIVE_TO_PARENT, 0f,
//                    TranslateAnimation.RELATIVE_TO_PARENT, 1.0f);
//            mAnimation.setDuration(10000);
//            mAnimation.setRepeatCount(-1);
//            // mAnimation.setRepeatMode(Animation.REVERSE);
//            mAnimation.setInterpolator(new LinearInterpolator());
//            imgV[i].setAnimation(mAnimation);
        }
        img=(ImageView)findViewById(R.id.imageView1);
        String[] imageArray = {"watermelon","apple","orange","strawberry"};
        Random rand = new Random();
        Random sx = new Random();
        int x = sx.nextInt(720 - 0 + 1);
        img.setX(x);
        x_value = x;
        int rndInt = rand.nextInt(4);
        int resID = getResources().getIdentifier(imageArray[rndInt], "drawable",  getPackageName());
        img.setImageResource(resID);



    mAnimation = new TranslateAnimation(
    TranslateAnimation.ABSOLUTE, 0f,
    TranslateAnimation.ABSOLUTE, 0f,
    TranslateAnimation.RELATIVE_TO_PARENT, 0f,
    TranslateAnimation.RELATIVE_TO_PARENT, 1.0f);
    mAnimation.setDuration(10000);
    mAnimation.setRepeatCount(-1);

    // mAnimation.setRepeatMode(Animation.REVERSE);
    mAnimation.setInterpolator(new LinearInterpolator());
    img.setAnimation(mAnimation);




    }
    //catch touch events

    public boolean onTouchEvent(MotionEvent event) {
//        int index = event.getActionIndex();
        System.out.println("coords "+event.getY()+" "+y);
        if ((event.getY() >= (y - 200) && event.getY() <= (y + 200)) && (event.getX() >= (x_value - 100) && event.getX() <= (x_value + 100)))
        {
            y = 0;
            scr += 10;
            score.setText(String.valueOf(scr));
           displayImages();
        }


        //2nd image
        if ((event.getY() >= (y2 - 200) && event.getY() <= (y2 + 200)) && (event.getX() >= (xv2 - 100) && event.getX() <= (xv2 + 100)))
        {
            y2 = 0;
            scr += 10;
            score.setText(String.valueOf(scr));
            displayImage2();
        }

        //3rd image
        if ((event.getY() >= (y3 - 200) && event.getY() <= (y3 + 200)) && (event.getX() >= (xv3 - 100) && event.getX() <= (xv3 + 100)))
        {
            y3 = 0;
            scr += 10;
            score.setText(String.valueOf(scr));
            displayImage3();
        }

        //mAnimation.cancel();
        //img.setVisibility(View.INVISIBLE);
        return true;
    }

}
